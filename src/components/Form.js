import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Text,
  TextInput,
  TouchableOpacity,
  Alert,
  PermissionsAndroid
} from "react-native";
import { Actions } from "react-native-router-flux";
import AsyncStorage from "@react-native-community/async-storage";
import API from "../API";
import IMEI from "react-native-imei";
class Form extends Component {
  state = {
    email: "",
    password: "",
    baseURL: API.baseURL,
    imeiNo: 0
  };

  async requestStatePermission() {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.READ_PHONE_STATE,
        {
          title: "Cool Photo App Camera Permission",
          message:
            "Cool Photo App needs access to your camera " +
            "so you can take awesome pictures.",
          buttonNeutral: "Ask Me Later",
          buttonNegative: "Cancel",
          buttonPositive: "OK"
        }
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log("You can use the camera");
      } else {
        console.log("Camera permission denied");
      }
    } catch (err) {
      console.warn(err);
    }
  }

  getImeiNo = async () => {
    await IMEI.getImei().then(imeiList => {
      this.setState({ imeiNo: parseInt(imeiList[0]) });
    });
  };

  handleSubmit = formType => {
    const { email, password, baseURL, imeiNo } = this.state;

    if (email === "" || password === "") {
      Alert.alert("Error", "Please check the credentials", [
        {
          text: "Okay"
        }
      ]);
      return false;
    }

    // email Validation
    if (!/^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email)) {
      Alert.alert("Error", "Please enter a valid email.", [
        {
          text: "Okay"
        }
      ]);
      return false;
    }

    // now check the email and password exist or not
    if (formType === "Login") {
      fetch(`${baseURL}/users/login`, {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json"
        },
        body: JSON.stringify({
          email: email,
          password: password,
          imeiNo: imeiNo
        })
      })
        .then(response => response.json())
        .then(responseJson => {
          if (responseJson.response === "success") {
            AsyncStorage.setItem(
              "@SMS:deviceID",
              JSON.stringify(responseJson.deviceId)
            );
            AsyncStorage.setItem(
              "@SMS:token",
              JSON.stringify(responseJson.token)
            );
            Actions.dashboard();
          }
        })
        .catch(error => console.error(error));
    } else if (formType === "Signup") {
      fetch(`${baseURL}/users/create`, {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json"
        },
        body: JSON.stringify({
          email: email,
          password: password,
          imeiNo: imeiNo
        })
      })
        .then(response => response.json())
        .then(responseJson => {
          if (responseJson.response === "success") {
            Alert.alert("Success", "Please login.", [
              {
                text: "Okay"
              }
            ]);
            Actions.login();
          }
        })
        .catch(error => console.error(error));
    }
  };

  async componentDidMount() {
    await this.requestStatePermission();
    await this.getImeiNo();
    AsyncStorage.getItem("@SMS:deviceID", (err, item) => {
      if (item) {
        Actions.dashboard();
      }
    });
  }

  render() {
    const { type } = this.props;
    return (
      <View style={styles.container}>
        <TextInput
          placeholder="Email"
          style={styles.input}
          underlineColorAndroid="rgba(0,0,0,0)"
          placeholderTextColor="#fff"
          selectionColor="#fff"
          keyboardType="email-address"
          onSubmitEditing={() => this.password.focus()}
          onChangeText={email => this.setState({ email })}
          value={this.state.email}
        />
        <TextInput
          placeholder="Password"
          style={styles.input}
          secureTextEntry={true}
          placeholderTextColor="#fff"
          underlineColorAndroid="rgba(0,0,0,0)"
          ref={input => (this.password = input)}
          onChangeText={password => this.setState({ password })}
          value={this.state.password}
        />
        <TouchableOpacity
          style={styles.button}
          onPress={() => this.handleSubmit(type)}
        >
          <Text style={styles.buttonText}>{type}</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

export default Form;

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    alignItems: "center",
    justifyContent: "center"
  },
  input: {
    width: 300,
    backgroundColor: "rgba(255,255,255,0.4)",
    borderRadius: 25,
    paddingHorizontal: 16,
    marginVertical: 10,
    fontSize: 16,
    color: "#fff"
  },
  button: {
    width: 300,
    backgroundColor: "#1c313a",
    borderRadius: 25,
    marginVertical: 10,
    paddingVertical: 12
  },
  buttonText: {
    fontSize: 16,
    fontWeight: "500",
    color: "#fff",
    textAlign: "center"
  }
});
