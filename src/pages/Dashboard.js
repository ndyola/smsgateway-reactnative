import React, { Component } from "react";
import {
  Text,
  StyleSheet,
  View,
  TouchableOpacity,
  FlatList
} from "react-native";
import API from "../API";
import { Actions } from "react-native-router-flux";
import moment from "moment";
import Asms from "react-native-sms-android";
import AsyncStorage from "@react-native-community/async-storage";
import IMEI from "react-native-imei";

class Dashboard extends Component {
  state = {
    dataSource: [],
    baseURL: API.baseURL,
    server: 0,
    deviceId: 0,
    token: 0
  };

  componentDidMount() {
    IMEI.getImei().then(imeiList => {
      console.warn(imeiList);
    });
    AsyncStorage.getItem("@SMS:deviceID").then(value =>
      this.setState({ deviceId: value })
    );
    AsyncStorage.getItem("@SMS:token").then(value =>
      this.setState({ token: value })
    );
    setInterval(() => {
      this.getAllData();
    }, 100);
    AsyncStorage.getItem("@SMS:server").then(value => {
      this.sendPendingData();
      this.setState({ server: value });
      setInterval(() => {
        this.sendPendingData();
      }, 10000);
    });
  }

  createServer = () => {
    if (this.state.server === 0) {
      AsyncStorage.setItem("@SMS:server", 1);
      this.setState({ server: 1 });
      setInterval(() => {
        this.sendPendingData();
      }, 10000);
    }
  };

  sendSMS = async responseJson => {
    const { baseURL } = this.state;
    if (responseJson.length > 0) {
      for (var i = 0; i < responseJson.length; i++) {
        let data = {
          inbox_messages: [],
          device_id: null
        };
        let inboxData = {
          id: null,
          message_id: null,
          device_id: null,
          phone_number: null,
          messages: null,
          status: null,
          log: null,
          createdAt: null,
          updatedAt: null
        };
        inboxData.id = responseJson[i].id;
        inboxData.message_id = responseJson[i].message_id;
        inboxData.device_id = responseJson[i].device_id;
        inboxData.phone_number = responseJson[i].phone_number;
        inboxData.messages = responseJson[i].messages;
        inboxData.status = "sent";
        let log = JSON.parse(responseJson[i].log);
        let newLogs = [];
        newLogs.push(...log, {
          status: "sent",
          occurred_at: moment().format()
        });
        inboxData.log = JSON.stringify(newLogs);
        inboxData.createdAt = responseJson[i].createdAt;
        inboxData.updatedAt = responseJson[i].updatedAt;

        data.device_id = responseJson[i].device_id;
        data.inbox_messages.push(inboxData);

        /**
         * SEND SMS
         */
        await Asms.sms(
          responseJson[i].phone_number, // phone number to send sms to
          responseJson[i].messages, // sms body
          "sendDirect", // sendDirect or sendIndirect
          (err, message) => {
            if (err) {
              console.warn("error", err);
            } else {
              fetch(`${baseURL}/messages/sendInboxText`, {
                method: "POST",
                headers: {
                  Accept: "application/json",
                  "Content-Type": "application/json"
                },
                body: JSON.stringify(data)
              })
                .then(response => response.json())
                .then(responseJson => {})
                .catch(error => console.error(error));
            }
          }
        );
      }
    }
  };

  sendPendingData = () => {
    const { baseURL, deviceId, token } = this.state;
    // Read Pending sms from Gateway
    fetch(`${baseURL}/messages/${deviceId}`, {
      method: "GET",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      }
    })
      .then(response => response.json())
      .then(responseJson => {
        if (responseJson.response === "success") {
          if (responseJson.messages.length === 0) return false;
          // Send SMS
          this.sendSMS(responseJson.messages);
        }
      })
      .catch(error => console.error(error));
  };

  getAllData = () => {
    const { deviceId, token, baseURL } = this.state;
    fetch(`${baseURL}/messages/getReceivedData/${deviceId}`, {
      method: "GET",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      }
    })
      .then(response => response.json())
      .then(responseJson => {
        if (responseJson.response === "success")
          this.setState({ dataSource: responseJson.messages });
      });
  };

  render() {
    return (
      <View style={styles.container}>
        <TouchableOpacity
          style={styles.button}
          onPress={() => this.createServer()}
        >
          <Text style={styles.buttonText}>Create Server</Text>
        </TouchableOpacity>
        <FlatList
          style={styles.flatContainer}
          data={this.state.dataSource}
          keyExtractor={(item, i) => i.toString()}
          renderItem={({ item }) => (
            <View style={styles.listContainer}>
              <Text style={styles.text}>{item.phone_number}</Text>
              <Text style={styles.text}>{item.messages}</Text>
              <Text style={styles.text}>{item.updatedAt}</Text>
            </View>
          )}
        />
      </View>
    );
  }
}

export default Dashboard;

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#455a64",
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  },
  button: {
    width: 200,
    backgroundColor: "#1c313a",
    borderRadius: 25,
    marginVertical: 15,
    paddingVertical: 12
  },
  buttonText: {
    fontSize: 16,
    fontWeight: "500",
    color: "#fff",
    textAlign: "center"
  },
  flatContainer: {
    backgroundColor: "rgba(0,0,0,0)"
  },
  listContainer: {
    backgroundColor: "#1c313a",
    marginVertical: 10,
    paddingHorizontal: 15
  },
  text: {
    fontSize: 16,
    color: "rgba(255,255,255,0.7)",
    marginVertical: 5
  }
});
