import React from "react";
import { StyleSheet, View, Text, TextInput, Image } from "react-native";

const Logo = () => {
  return (
    <View style={styles.container}>
      <Image
        style={{ width: 50, height: 50 }}
        source={{
          uri: "https://facebook.github.io/react-native/img/header_logo.png"
        }}
      />
      <Text style={styles.logoText}>Welcome to SMSGateway</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  logoText: {
    marginVertical: 15,
    fontSize: 18,
    color: "rgba(255,255,255,0.7)"
  }
});

export default Logo;
