import React from "react";
import { StyleSheet, View, Text, DrawerLayoutAndroid } from "react-native";
import Logo from "../components/Logo";
import Form from "../components/Form";
import { Actions } from "react-native-router-flux";

const Sidebar = () => {
  var navigationView = (
    <View style={{ flex: 1, backgroundColor: "#fff" }}>
      <Text style={{ margin: 10, fontSize: 15, textAlign: "left" }}>
        I'm in the Drawer!
      </Text>
    </View>
  );
  return (
    <DrawerLayoutAndroid
      drawerWidth={300}
      drawerPosition={DrawerLayoutAndroid.positions.Left}
      renderNavigationView={() => navigationView}
    >
      <View style={{ flex: 1, alignItems: "center" }}>
        <Text style={{ margin: 10, fontSize: 15, textAlign: "right" }}>
          Hello
        </Text>
        <Text style={{ margin: 10, fontSize: 15, textAlign: "right" }}>
          World!
        </Text>
      </View>
    </DrawerLayoutAndroid>
  );
};

export default Sidebar;
