import React from "react";
import { StyleSheet, View, Text, TouchableOpacity } from "react-native";
import Logo from "../components/Logo";
import Form from "../components/Form";
import { Actions } from "react-native-router-flux";

const Login = () => {
  return (
    <View style={styles.container}>
      <Logo />
      <Form type="Login" />
      <View style={styles.signupTextContent}>
        <Text style={styles.signupText}>Don't have an account yet?</Text>
        <TouchableOpacity onPress={() => Actions.register()}>
          <Text style={styles.signupButton}>Signup</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#455a64",
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  },
  signupTextContent: {
    flexGrow: 1,
    alignItems: "flex-end",
    justifyContent: "center",
    paddingVertical: 16,
    flexDirection: "row"
  },
  signupText: {
    fontSize: 16,
    color: "rgba(255,255,255,0.7)"
  },
  signupButton: {
    fontSize: 16,
    color: "#fff",
    fontWeight: "500"
  }
});

export default Login;
